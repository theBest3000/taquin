/*
 * Copyright (C) 2022  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.LocalStorage 2.7

import "statdb.js" as Statdb

Page {
    id: statPage
    visible: false
    header: PageHeader {
        id: headerInfo
        title: i18n.tr('Game Statistics')
    }

    readonly property int cstColnb: 4           // to set according to number of displayed columns in the ListView
    readonly property var cstmarginRatio: 10    // to define margin for text in a Rectangle of the ListView
    
    property var values : {
        return Statdb.ReadStatsfromDB(10)
    }
    
    Component.onCompleted: {
        console.log("Values:",values)
        
        statModel.clear()
        
        for (var i = 0; i < values.length ; i=i+9) {
            statModel.append({"RowNb":values[i+1], "ColNb":values[i+2], "Win":values[i+3], "Canceled":values[i+4], "MinMoves":values[i+5], "MaxMoves":values[i+6], "TotalWinMoves":values[i+7], "TotalCancelMoves":values[i+8]})
        }
    }
    
    ListModel {
        id: statModel
        ListElement {   // list for example only, data are cleared on completed and replaced by actual values from the database
            RowNb: 4
            ColNb: 4
            Win: 10
            Canceled: 20
            MinMoves: 100
            MaxMoves: 200            // not used, may be for future use
            TotalWinMoves: 1000      // not used, may be for future use
            TotalCancelMoves: 2000   // not used, may be for future use
        }
    }
    
    ListView {
        id: thelistv
        anchors.top: headerInfo.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - units.gu(1)
        height: parent.height - headerInfo.height
        header: headerComponent
        model: statModel
        delegate: contactDelegate
        
        Component {
            id: headerComponent
            Row {
                id: titlerow
                 Rectangle {
                    color: theme.palette.normal.activity
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(8)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: theme.palette.normal.activityText
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        wrapMode: Text.Wrap
                        text: i18n.tr("Row x Column")
                    }
                }
                Rectangle {
                    color: theme.palette.normal.activity
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(8)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: theme.palette.normal.activityText
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        wrapMode: Text.Wrap
                        text: i18n.tr("Canceled")
                    }
                }
                Rectangle {
                    color: theme.palette.normal.activity
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(8)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: theme.palette.normal.activityText
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        wrapMode: Text.Wrap
                        text: i18n.tr("Won")
                    }
                }
                Rectangle {
                    color: theme.palette.normal.activity
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(8)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: theme.palette.normal.activityText
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        wrapMode: Text.Wrap
                        text: i18n.tr("Best moves")
                    }
                }
            }
        }
        
        Component {
            id: contactDelegate
            Row {
                Rectangle {
                    color: {
                        if ((RowNb == maxRow) && (ColNb == maxCol))
                            return theme.palette.normal.positive
                        else
                            return theme.palette.normal.base
                    }
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(4)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: {
                            if ((RowNb == maxRow) && (ColNb == maxCol))
                                return theme.palette.normal.positiveText
                            else
                                return theme.palette.normal.baseText
                        }
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        wrapMode: Text.Wrap
                        text: RowNb + "x" + ColNb
                    }
                }
                Rectangle {
                    color: {
                        if ((RowNb == maxRow) && (ColNb == maxCol))
                            return theme.palette.normal.positive
                        else
                            return theme.palette.normal.base
                    }
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(4)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: {
                            if ((RowNb == maxRow) && (ColNb == maxCol))
                                return theme.palette.normal.positiveText
                            else
                                return theme.palette.normal.baseText
                        }
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.Wrap
                        text: Canceled
                    }
                }
                Rectangle {
                    color: {
                        if ((RowNb == maxRow) && (ColNb == maxCol))
                            return theme.palette.normal.positive
                        else
                            return theme.palette.normal.base
                    }
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(4)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: {
                            if ((RowNb == maxRow) && (ColNb == maxCol))
                                return theme.palette.normal.positiveText
                            else
                                return theme.palette.normal.baseText
                        }
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true                        
                        wrapMode: Text.Wrap
                        text: Win
                    }
                }
                Rectangle {
                    color: {
                        if ((RowNb == maxRow) && (ColNb == maxCol))
                            return theme.palette.normal.positive
                        else
                            return theme.palette.normal.base
                    }
                    border.color: theme.palette.normal.backgroundText
                    border.width: 1
                    width: thelistv.width / cstColnb
                    height: units.gu(4)
                    Label {
                        anchors.fill: parent
                        anchors.margins: parent.width / cstmarginRatio
                        color: {
                            if ((RowNb == maxRow) && (ColNb == maxCol))
                                return theme.palette.normal.positiveText
                            else
                                return theme.palette.normal.baseText
                        }
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.Wrap
                        text: MinMoves
                    }
                }
            }
        }        
    }
    
    Component.onDestruction: {
        console.log("Destruction StatPage")
    }        
}
