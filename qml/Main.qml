/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import QtQuick.LocalStorage 2.7
import Qt.labs.settings 1.0
import Qt.labs.platform 1.0
import FileMngt 1.0

import "taquin.js" as Taquin
import "statdb.js" as Statdb


MainView {
    id: mainView
    objectName: 'mainView'
    applicationName: 'taquin.aloysliska'
    automaticOrientation: true

    width: units.gu(50)
    height: units.gu(75)
    
    /************************/
    /* Application settings */
    /************************/

    /* The puzzle is composed of tiles arranged in rows and columns.
     * Mathematically, the position of these tiles are represented with a matrix or a table
     * In the code, this matrix (or table) is stored as a one dimensional array named tileTable
     */
    property int maxRow: 4                 // number of rows initialized to defaut value
    property int maxCol: 4                 // number of columns initialized to default value
    property int maxRowIndex: maxRow - 1   // maximum index for row
    property int maxColIndex: maxCol - 1   // maximum index for column
    property int tabTsize: maxRow*maxCol   // number of element in the matrix (or in the table)

    // Tile geometry : square or rectangle
    property bool squareTile: true

    // Tile border
    property bool borderTile: true
    
    // Index of selected image. Value zero is for no image but numbers
    property int indexImg: 0
    
    // url of user imported image
    property string myImageUrl: ""

    /********************************************/
    /* To save and restore application settings */
    /********************************************/
    Settings { 
        // note: it is important to define maxRow as a int, and not a var, otherwise when settings are restored, type is a string which will cause type error
        id: appSettings
        property alias maxRow: mainView.maxRow
        property alias maxCol: mainView.maxCol
        property alias squareTile: mainView.squareTile
        property alias borderTile: mainView.borderTile        
        property alias indexImg: mainView.indexImg
        property alias myImageUrl: mainView.myImageUrl
    }
    
    
    /****************************************************************/
    /* Game state variables loaded from json file or set to default */
    /****************************************************************/    
    // indicator of game over
    property bool gameOver: {
        FileMngt.loadJsonGameState("gameOver", false)
    }

    // counting of move
    property int movcount: {
        FileMngt.loadJsonGameState("movcount", 0)
    }

    // matrix definition
    property var tileTable: {
        if (FileMngt.loadJsonGameState("tileTable") == 0) {
            console.log("Create new matrix")
            return Taquin.createMatrix(maxRow,maxCol)
        }
        else {
            console.log("Load matrix from json file")
            return FileMngt.loadJsonGameState("tileTable")
        }
    }
    
    /************************************************/
    /* Function to save the game state in json file */
    /************************************************/
    function saveGameState() {       
        console.log("Save game state")
        FileMngt.saveJsonGameState("gameOver", gameOver)
        FileMngt.saveJsonGameState("movcount", movcount)
        FileMngt.saveJsonGameState("tileTable", tileTable)
        FileMngt.saveJsonFile()
    }
    
    /************************/
    /* Other game variables */
    /************************/    
    // definition of the free tile: this is an array [i,j] which are indexes in the matrix
    property var currFreeTile: Taquin.initFreeTile()
        
    property int initFreeIndex: 0  // index of free tile for access in the repeater of the tiles
    
    // to display numbers in the tiles
    property bool dispNumb: false
    
    // state of application : active or not active
    property bool applicationActive: Qt.application.state === Qt.ApplicationActive
    
    
    /*************/
    /* Constants */
    /*************/
    // List of images
    readonly property var imageSelecTableText: [i18n.tr("Numbers"),i18n.tr("Flower"),i18n.tr("Mountain"),i18n.tr("Rainbow"), i18n.tr("My image")]
    readonly property var imageSelecTable: [0,1,2,3,4]  
    readonly property int indexMyImage: imageSelecTable.length - 1     // index in imageSelecTable for "My image"
    

    /*************/
    PageStack {
        id: pageStack

        // signal to request a new game
        signal newGame()
        
        // Action on new game. 
        //  Main purpose of stack clear and push is to rebuild entirely the repeater of tiles in MainPage.
        onNewGame: {
            pageStack.clear()
            
            // if game canceled, save statistics for canceled game
            if ((gameOver == false) && (movcount != 0)) {
                console.log("SAVE CANCEL GAME. gameOver false AND movCount !=0")
                Statdb.SaveStatsinDB(gameOver)
            }
            
            Taquin.newGame()
            pageStack.push(Qt.resolvedUrl("MainPage.qml"))
        }
        
        Component.onCompleted: {
            Statdb.CreateDBtable()
            pageStack.push(Qt.resolvedUrl("MainPage.qml"))
        }    
    }

    Component.onDestruction: {
        // BUG Issue #7: onDestruction event is not called when closing the app.
        //      So saveGameState() is not done.
        //      In order to save game state, saving is done on onApplicationActiveChanged (which is in fact not strictly necessary)
        //      because saving is also done on application exit with onAboutToQuit.
        saveGameState()
        console.log("Destruction Mainview")
    }
    
    // Partial workaround to fix Issue #7: save game state when application is no more active
    onApplicationActiveChanged: {
        if (!applicationActive) {
            saveGameState()
            console.log("Application NOT Active:", Qt.application.state)
        }
        else {
            console.log("Application Active:", Qt.application.state)
        }
    }
    
    // Workaround to fix Issue #8: perform saving of game state on signal AboutToQuit() from Qt.application
    Connections {
        target: Qt.application
        onAboutToQuit: {
            saveGameState()  
            console.log("Quit")
            // note there was previsouly BUG Issue #8: the configuration file (taquin.aloysliska.conf) was not updated
        }
    }
}
