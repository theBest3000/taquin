/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "filemngt.h"

void FileMngtPlugin::registerTypes(const char *uri) {
    //@uri FileMngt
    qmlRegisterSingletonType<FileMngt>(uri, 1, 0, "FileMngt", [](QQmlEngine*, QJSEngine*) -> QObject* { return new FileMngt; });
}
